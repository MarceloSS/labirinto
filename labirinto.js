const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];//gerando player
let player = document.getElementById("Player")
//geração do mapa
let base = document.getElementById("base")
let xyWalls = []
let xyFinish = []
for(line in map){
    let thisLine = map[line].split("")
    let row = document.createElement("div")
    row.className = "Line"
    for(l in thisLine){
        switch(thisLine[l]){
            case "W":
                let wall = document.createElement("div")
                wall.className = "Wall"
                row.appendChild(wall)
                let wallLoc = [line, l]
                xyWalls.push(wallLoc)
                break
            case " ":
                let space = document.createElement("div")
                space.className = "Space"
                row.appendChild(space)
                break
            case "S":
                row.appendChild(player)
                let start = document.createElement("div")
                start.className = "Start"
                row.appendChild(start)
                break
            case "F":
                let final = document.createElement("div")
                final.className = "Final"
                row.appendChild(final)
                xyFinish = [line, l]
                break
        }
    }
    base.appendChild(row)
}
console.log(xyWalls[1])
//movimentação do jogador
let playerTop = 9;
let playerLeft = 0;

document.addEventListener('keydown', (event) => {
    const keyName = event.key;
    console.log(playerTop, playerLeft)
    console.log('keydown event\n\n' + 'key: ' + keyName);
    switch(keyName){
        case "ArrowUp":
            if(!check(playerTop-1,playerLeft)){
                playerTop -= 1
            }
            break
        case "ArrowDown":
            if(!check(playerTop+1,playerLeft)){
                playerTop += 1
            }
            break
        case "ArrowRight":
            if(!check(playerTop,playerLeft+1)){
                playerLeft += 1
            }
            break
        case "ArrowLeft":
            if(!check(playerTop,playerLeft-1)&& playerLeft-1 >= 0){
                playerLeft -= 1
            }
            break
  }
  Player.style.top = playerTop*20 + "px";
  Player.style.left = playerLeft*20 + "px";
    if(playerTop == xyFinish[0] && playerLeft == xyFinish[1]){
        alert("YOU WIN")
        playerTop = 9;
        playerLeft = 0;
    }
});
//checando paredes
function check(plTop, plLeft){
    for(walls in xyWalls){
        if(plTop == xyWalls[walls][0] && plLeft == xyWalls[walls][1]){
            return true
        }
    }
    return false
}